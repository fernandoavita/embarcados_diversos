
// this code was based on  
// https://github.com/adafruit/DHT-sensor-library/blob/master/examples/DHT_Unified_Sensor/DHT_Unified_Sensor.ino

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <LiquidCrystal.h>

#define DHTPIN         8     // Digital pin connected to the DHT sensor 
#define DHTTYPE    DHT22     // DHT 22 (AM2302)

DHT_Unified dht(DHTPIN, DHTTYPE);
LiquidCrystal lcd(2, 3, 4, 5, 6, 7);

uint32_t delayMS;
sensors_event_t event;

void setup() {
  sensor_t sensor;
  
  Serial.begin(9600);
  lcd.begin(16, 2);
  dht.begin();
  lcd.print("Starting...");
/*
  Serial.println(F("DHT22 Details"));
  Serial.println(F("------------------------------------"));
  Serial.println(F("Temperature Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("°C"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("°C"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("°C"));
  Serial.println(F("------------------------------------"));
*/  
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
/*  
  Serial.println(F("Humidity Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("%"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("%"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("%"));
  Serial.println(F("------------------------------------"));
*/
  // Set delay between sensor readings based on sensor details.
  delayMS = sensor.min_delay / 1000;

  lcd.clear();
  lcd.print(F("Temp:        C"));
  lcd.setCursor(0,1);
  lcd.print(F("Humid:       %"));
}

void loop() {

  // Get temperature event and print its value.
  Serial.print(F("Temperature: "));
  lcd.setCursor(7,0);  
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) 
  {
    Serial.println(F("Error"));
    lcd.print(F("Error"));
  }
  else 
  {
    Serial.print(event.temperature);
    Serial.println(F("C"));

    lcd.print(event.temperature);
  }

  // Get humidity event and print its value.
  Serial.print(F("Humidity: "));
  lcd.setCursor(7,1);
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) 
  {
    Serial.println(F("Error"));
    lcd.print(F("Error"));
  }
  else 
  {
    Serial.print(event.relative_humidity);
    Serial.println(F("%"));
    
    lcd.print(event.relative_humidity);
  }
  
  delay(delayMS);
}

